---
title: Urda's Projects and Programming
layout: page
permalink: /projects/
---

I build and tinker with computer engineering and computer science from time to time.

You can find my GitHub profile at:

## [github.com/urda](https://github.com/urda)

Here's my offerings:

- [Advent of Code](https://github.com/urda/advent-of-code) - I sometimes try my hand at the Advent of Code.
- [django-letsencrypt](https://github.com/urda/django-letsencrypt) - A simple Django app to handle Let's Encrypt ACME challenges.
- [Opinionated Minecraft Datapack](https://github.com/urda/opinionated-minecraft-datapack) - I have some "opinions" on a few vanilla tweaks and features for vanilla Minecraft.
- [Python NIST Randomness Beacon](https://github.com/urda/nistbeacon) - Python 3 Library to access the NIST Randomness Beacon.
- [Quick Start Delight](https://github.com/urda/quick-start-delight) - Factorio mod to add a "starter kit" of supplies for new games.
- [urda.bash](https://github.com/urda/urda.bash) - You can see my `bash` shell modifications here.
- [urda/website at GitHub](https://github.com/urda/website) - See how this website is put together.
